import config
import numpy as np
import cv2

class ColourDetector:
    def __init__(self, image, output_image_path):
        self.image = image
        self.output_image_path = output_image_path
        self.coordinates = []

    def detect_pathogens(self):
        apple_mildew_mask = self.apple_mildew_mask()
        apple_scab_mask   = self.apple_scab_mask()

        self.get_position(apple_mildew_mask, "apple_mildew")
        self.get_position(apple_scab_mask, "apple_scab")
        return self.coordinates

    def get_position(self, mask, pathogen):

        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        for contour in contours:
            area = cv2.contourArea(contour)
            if area > config.MIN_AREA:
                M = cv2.moments(contour)
                X = int(M["m10"] / M["m00"])
                Y = int(M["m01"] / M["m00"])
                self.coordinates.append((X, Y, pathogen))

    def apple_mildew_mask(self):
        image = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)

        lower = config.BEIGE_LOWER
        upper = config.BEIGE_UPPER
        lower = np.array(lower, dtype = "uint8")
        upper = np.array(upper, dtype = "uint8")
        mask = cv2.inRange(image, lower, upper)
        return mask

    def apple_scab_mask(self):
        image = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)

        lower = config.GOLDEN_LOWER
        upper = config.GOLDEN_UPPER
        lower = np.array(lower, dtype = "uint8")
        upper = np.array(upper, dtype = "uint8")
        mask = cv2.inRange(image, lower, upper)
        return mask

    def save_outpu_image(self):
        output_image = self.image.copy()

        for coordinate in self.coordinates:
            X, Y, pathogen = coordinate
            cv2.circle(output_image,(X,Y),2,(0,255,0),5)
            cv2.putText(output_image,pathogen,(X+5,Y),cv2.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)
        cv2.imwrite(self.output_image_path, output_image)

