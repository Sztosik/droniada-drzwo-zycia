import json

def save_json(ned_coordinates):
    POINTS = {
        "type": "PointsCollection",
        "points": []
    }
    
    i=0
    for coordinate in ned_coordinates:
        i+=1
        north, east, pathogen = coordinate
        POINT = {
            "type": "Point",
            "number": i,
            "pathogen": pathogen,
            "coordinates": [north, east]
        }
        POINTS["points"].append(POINT)

    with open('points.json', 'w') as f:
        json.dump(POINTS, f, indent=2)
