import os
import config
import list2json
from image_classifier import ImageClassifier

image_directory = os.fsencode(config.IMAGE_DIR)
ned_coordinates = []

for file in os.listdir(image_directory):
    filename = os.fsdecode(file)
    image_classifier = ImageClassifier(filename=filename, ned_coordinates=ned_coordinates)
    image_classifier.start_image_processing()

list2json.save_json(ned_coordinates)

print('done')

