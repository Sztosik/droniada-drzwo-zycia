import config
import cv2
import imutils
from colour_detector import ColourDetector
from util_img2geo import Camera
import util_img2geo

class ImageClassifier:
    def __init__(self, filename, ned_coordinates):
        self.filename = filename
        self.image_path = f"{config.IMAGE_DIR}/{filename}"
        self.output_image_path = f"{config.OUTPUT_IMAGES_DIR}/{filename}"
        self.ned_coordinates = ned_coordinates
    
    def start_image_processing(self):
        image = self.read_image()
        image = self.resize_image(image)
        h, w, c = image.shape

        camera = Camera(focal=config.FOCAL, sensorw=config.SENSORW, sensorh=config.SENSORH, resx=w, resy=h)
        colour_detector = ColourDetector(image=image, output_image_path=self.output_image_path)

        xy_coordinates = colour_detector.detect_pathogens()
        colour_detector.save_outpu_image()
        self.xy2ned(xy_coordinates, camera)

    def xy2ned(self, xy_coordinates, camera):
        for coordinate in xy_coordinates:
            img_x, img_y, pathogen = coordinate
            north, east = camera.get_ned_pos(img_x, img_y)
            
            if self.distance_bettween_points(north, east):
                self.ned_coordinates.append((north, east, pathogen))
                print(north, east, pathogen)

    def distance_bettween_points(self, n1, e1):
        for coordinate in self.ned_coordinates:
            n, e, pathogen = coordinate
            distance = util_img2geo._get_distance_between_points(n, e, n1, e1)
            if distance < config.MIN_DISTANCE_BETWEEN_POINTS:
                return False
        return True

    def read_image(self):
        image = cv2.imread(self.image_path)
        return image

    def resize_image(self, image):
        img_resized = imutils.resize(image, width=config.RESIZED_WIDTH)
        return img_resized
            
